package ua.khpi.oop.labs.kryzha09;

/**
 * 9 laboratory work
 * @version 13 Mar 2023
 * @author Illia Kryzhanovskyi
 */
public class Main {
	/**
	 * The point of enter
	 */
	public static void main(String[] args) {
		AddressBookProgram program = AddressBookProgram.getInstance();
		program.start();
	}
}
