package ua.khpi.opp.labs.kryzha16;

/**
 * 15 laboratory work
 * Remake 14 lab for using Collections API
 * @author Ilya Kryzhanovskiy
 */
public class Main {
	/**
	 * The point of entrance
	 */
	public static void main(String[] args) {
		AddressBookApplication addressBookApplication = AddressBookApplication.getInstance();
		addressBookApplication.start();
	}
}
