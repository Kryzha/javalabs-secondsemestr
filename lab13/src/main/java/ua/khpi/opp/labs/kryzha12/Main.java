package ua.khpi.opp.labs.kryzha12;

/**
 * 13 laboratory work
 * Create multi threading environment for lab 12
 * @author Ilya Kryzhanovskiy
 */
public class Main {
	/**
	 * The point of entrance
	 */
	public static void main(String[] args) {
		AddressBookApplication addressBookApplication = AddressBookApplication.getInstance();
		addressBookApplication.start();
	}
}
