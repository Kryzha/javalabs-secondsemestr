executing command `java -jar lab13.jar`, the result:
```bash
Type here amount of notes
2
Please, type here address number: 1
st. Kharkiv
Please, type here phone number: 1
+380999463723
Please, type here address number: 2
st. Kharkiv
Please, type here phone number: 2
+380654753454
Seeding passed elements to your user storage...
Elements: {[[+380999463723], [st. Kharkiv]][[+380654753454], [st. Kharkiv]]}
Length: 2
Start multhithreading part
Please, type here number of threads
10
Please type here timeout for threads in seconds
1
13 => pool-1-thread-1 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 13
[[[+380999463723], [st. Kharkiv]]] => 13
[] => 13
13 => pool-1-thread-1 ended:  time: 67
22 => pool-1-thread-10 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 22
[[[+380999463723], [st. Kharkiv]]] => 22
[] => 22
22 => pool-1-thread-10 ended:  time: 14
21 => pool-1-thread-9 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 21
[[[+380999463723], [st. Kharkiv]]] => 21
[] => 21
21 => pool-1-thread-9 ended:  time: 14
20 => pool-1-thread-8 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 20
[[[+380999463723], [st. Kharkiv]]] => 20
[] => 20
20 => pool-1-thread-8 ended:  time: 13
19 => pool-1-thread-7 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 19
[[[+380999463723], [st. Kharkiv]]] => 19
[] => 19
19 => pool-1-thread-7 ended:  time: 14
18 => pool-1-thread-6 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 18
[[[+380999463723], [st. Kharkiv]]] => 18
[] => 18
18 => pool-1-thread-6 ended:  time: 14
17 => pool-1-thread-5 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 17
[[[+380999463723], [st. Kharkiv]]] => 17
[] => 17
17 => pool-1-thread-5 ended:  time: 13
16 => pool-1-thread-4 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 16
[[[+380999463723], [st. Kharkiv]]] => 16
[] => 16
16 => pool-1-thread-4 ended:  time: 14
15 => pool-1-thread-3 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 15
[[[+380999463723], [st. Kharkiv]]] => 15
[] => 15
15 => pool-1-thread-3 ended:  time: 15
14 => pool-1-thread-2 started
Finding all phone numbers in Kharkiv with Lifecell and Vodafone in thread number: 14
[[[+380999463723], [st. Kharkiv]]] => 14
[] => 14
14 => pool-1-thread-2 ended:  time: 14

Process finished with exit code 0
```