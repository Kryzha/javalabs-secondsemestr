package ua.khpi.opp.labs.kryzha12;

/**
 * 11 laboratory work
 * Create a validation via patterns and file reading to lab 10
 * @author Ilya Kryzhanovskiy
 */
public class Main {
	/**
	 * The point of entrance
	 */
	public static void main(String[] args) {
		AddressBookApplication addressBookApplication = AddressBookApplication.getInstance();
		addressBookApplication.start();
	}
}
