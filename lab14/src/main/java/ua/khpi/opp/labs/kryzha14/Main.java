package ua.khpi.opp.labs.kryzha14;

/**
 * 14 laboratory work
 * Compare multithreading environment to normal one
 * @author Ilya Kryzhanovskiy
 */
public class Main {
	/**
	 * The point of entrance
	 */
	public static void main(String[] args) {
		AddressBookApplication addressBookApplication = AddressBookApplication.getInstance();
		addressBookApplication.start();
	}
}
